﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypingSpeedTest
{
    public class TypingTestTableLayoutPanelDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public TypingTestTableLayoutPanelDesigner()
        {
            this.components = new System.ComponentModel.Container();
            this.flpWordsToType = new System.Windows.Forms.FlowLayoutPanel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblTimeLabel = new System.Windows.Forms.Label();
            this.lblSeconrdsLabel = new System.Windows.Forms.Label();
            this.tblpMainTable = new System.Windows.Forms.TableLayoutPanel();
            this.tblpFooter = new System.Windows.Forms.TableLayoutPanel();
            this.tblpButtonsRestartAndBack = new System.Windows.Forms.TableLayoutPanel();
            this.btnRestart = new System.Windows.Forms.Button();
            this.tblpTimeElapsedSeconds = new System.Windows.Forms.TableLayoutPanel();
            this.tbTimeElapsed = new System.Windows.Forms.TextBox();
            this.tblpWPM = new System.Windows.Forms.TableLayoutPanel();
            this.lblWPMText = new System.Windows.Forms.Label();
            this.lblWPMValue = new System.Windows.Forms.Label();
            this.tbInputLine = new System.Windows.Forms.TextBox();
            this.typingTestTimer = new System.Windows.Forms.Timer(components);

            //avoid the layout system repeatedly reacting to your changes. Instead perform only at the end.
            //https://stackoverflow.com/questions/3838315/c-sharp-why-use-suspendlayout
            this.tblpMainTable.SuspendLayout();
            this.tblpFooter.SuspendLayout();
            this.tblpButtonsRestartAndBack.SuspendLayout();
            this.tblpTimeElapsedSeconds.SuspendLayout();
            this.tblpWPM.SuspendLayout();
            // 
            // flpWordsToType
            // 
            this.flpWordsToType.AutoScroll = true;
            this.flpWordsToType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpWordsToType.Location = new System.Drawing.Point(0, 0);
            this.flpWordsToType.Margin = new System.Windows.Forms.Padding(0);
            this.flpWordsToType.Name = "flpWordsToType";
            this.flpWordsToType.Size = new System.Drawing.Size(630, 218);
            this.flpWordsToType.TabIndex = 3;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(3, 57);
            this.btnBack.MinimumSize = new System.Drawing.Size(75, 23);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;

            // 
            // lblTimeLabel
            // 
            this.lblTimeLabel.AutoSize = true;
            this.lblTimeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTimeLabel.Location = new System.Drawing.Point(3, 0);
            this.lblTimeLabel.Name = "lblTimeLabel";
            this.lblTimeLabel.Size = new System.Drawing.Size(46, 28);
            this.lblTimeLabel.TabIndex = 6;
            this.lblTimeLabel.Text = "Time:";
            this.lblTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSeconrdsLabel
            // 
            this.lblSeconrdsLabel.AutoSize = true;
            this.lblSeconrdsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSeconrdsLabel.Location = new System.Drawing.Point(107, 0);
            this.lblSeconrdsLabel.Name = "lblSecondsLabel";
            this.lblSeconrdsLabel.Size = new System.Drawing.Size(48, 28);
            this.lblSeconrdsLabel.TabIndex = 9;
            this.lblSeconrdsLabel.Text = "seconds";
            this.lblSeconrdsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblpMainTable
            // 
            this.tblpMainTable.ColumnCount = 1;
            this.tblpMainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpMainTable.Controls.Add(this.tblpFooter, 0, 2);
            this.tblpMainTable.Controls.Add(this.tbInputLine, 0, 1);
            this.tblpMainTable.Controls.Add(this.flpWordsToType, 0, 0);
            this.tblpMainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblpMainTable.Location = new System.Drawing.Point(0, 0);
            this.tblpMainTable.Name = "tblpMainTable";
            this.tblpMainTable.RowCount = 3;
            this.tblpMainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tblpMainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tblpMainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblpMainTable.Size = new System.Drawing.Size(630, 398);
            this.tblpMainTable.TabIndex = 6;
            // 
            // tblpFooter
            // 
            this.tblpFooter.ColumnCount = 3;
            this.tblpFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tblpFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tblpFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tblpFooter.Controls.Add(this.tblpButtonsRestartAndBack, 0, 0);
            this.tblpFooter.Controls.Add(this.tblpTimeElapsedSeconds, 2, 0);
            this.tblpFooter.Controls.Add(this.tblpWPM, 1, 0);
            this.tblpFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblpFooter.Location = new System.Drawing.Point(3, 280);
            this.tblpFooter.Name = "tblpFooter";
            this.tblpFooter.RowCount = 1;
            this.tblpFooter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpFooter.Size = new System.Drawing.Size(624, 115);
            this.tblpFooter.TabIndex = 10;
            // 
            // tblpButtonsRestartAndBack
            // 
            this.tblpButtonsRestartAndBack.AutoSize = true;
            this.tblpButtonsRestartAndBack.ColumnCount = 1;
            this.tblpButtonsRestartAndBack.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpButtonsRestartAndBack.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblpButtonsRestartAndBack.Controls.Add(this.btnBack, 0, 1);
            this.tblpButtonsRestartAndBack.Controls.Add(this.btnRestart, 0, 0);
            this.tblpButtonsRestartAndBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblpButtonsRestartAndBack.Location = new System.Drawing.Point(3, 3);
            this.tblpButtonsRestartAndBack.Name = "tblpButtonsRestartAndBack";
            this.tblpButtonsRestartAndBack.RowCount = 2;
            this.tblpButtonsRestartAndBack.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpButtonsRestartAndBack.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpButtonsRestartAndBack.Size = new System.Drawing.Size(199, 109);
            this.tblpButtonsRestartAndBack.TabIndex = 7;
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(3, 3);
            this.btnRestart.MinimumSize = new System.Drawing.Size(75, 23);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(75, 23);
            this.btnRestart.TabIndex = 6;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;

            // 
            // tblpTimeElapsedSeconds
            // 
            this.tblpTimeElapsedSeconds.ColumnCount = 3;
            this.tblpTimeElapsedSeconds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpTimeElapsedSeconds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tblpTimeElapsedSeconds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tblpTimeElapsedSeconds.Controls.Add(this.lblTimeLabel, 0, 0);
            this.tblpTimeElapsedSeconds.Controls.Add(this.lblSeconrdsLabel, 2, 0);
            this.tblpTimeElapsedSeconds.Controls.Add(this.tbTimeElapsed, 1, 0);
            this.tblpTimeElapsedSeconds.Dock = System.Windows.Forms.DockStyle.Right;
            this.tblpTimeElapsedSeconds.Location = new System.Drawing.Point(463, 3);
            this.tblpTimeElapsedSeconds.MaximumSize = new System.Drawing.Size(158, 28);
            this.tblpTimeElapsedSeconds.MinimumSize = new System.Drawing.Size(158, 28);
            this.tblpTimeElapsedSeconds.Name = "tblpTimeElapsedSeconds";
            this.tblpTimeElapsedSeconds.RowCount = 1;
            this.tblpTimeElapsedSeconds.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpTimeElapsedSeconds.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tblpTimeElapsedSeconds.Size = new System.Drawing.Size(158, 28);
            this.tblpTimeElapsedSeconds.TabIndex = 10;
            // 
            // tbTimeElapsed
            // 
            this.tbTimeElapsed.BackColor = System.Drawing.SystemColors.Window;
            this.tbTimeElapsed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tbTimeElapsed.Location = new System.Drawing.Point(55, 5);
            this.tbTimeElapsed.Name = "tbTimeElapsed";
            this.tbTimeElapsed.ReadOnly = true;
            this.tbTimeElapsed.Size = new System.Drawing.Size(46, 20);
            this.tbTimeElapsed.TabIndex = 5;
            // 
            // tblpWPM
            // 
            this.tblpWPM.ColumnCount = 1;
            this.tblpWPM.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpWPM.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblpWPM.Controls.Add(this.lblWPMText, 0, 0);
            this.tblpWPM.Controls.Add(this.lblWPMValue, 0, 1);
            this.tblpWPM.Dock = System.Windows.Forms.DockStyle.Top;
            this.tblpWPM.Location = new System.Drawing.Point(208, 3);
            this.tblpWPM.Name = "tblpWPM";
            this.tblpWPM.RowCount = 2;
            this.tblpWPM.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpWPM.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpWPM.Size = new System.Drawing.Size(206, 100);
            this.tblpWPM.TabIndex = 11;
            // 
            // lblWPMText
            // 
            this.lblWPMText.AutoSize = true;
            this.lblWPMText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWPMText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblWPMText.Location = new System.Drawing.Point(3, 0);
            this.lblWPMText.Name = "lblWPMText";
            this.lblWPMText.Size = new System.Drawing.Size(200, 50);
            this.lblWPMText.TabIndex = 0;
            this.lblWPMText.Text = "WPM";
            this.lblWPMText.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblWPMValue
            // 
            this.lblWPMValue.AutoSize = true;
            this.lblWPMValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWPMValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblWPMValue.Location = new System.Drawing.Point(3, 50);
            this.lblWPMValue.Name = "lblWPMValue";
            this.lblWPMValue.Size = new System.Drawing.Size(200, 50);
            this.lblWPMValue.TabIndex = 1;
            this.lblWPMValue.Text = "0";
            this.lblWPMValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tbInputLine
            // 
            this.tbInputLine.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbInputLine.Location = new System.Drawing.Point(3, 221);
            this.tbInputLine.Name = "tbInputLine";
            this.tbInputLine.Size = new System.Drawing.Size(624, 20);
            this.tbInputLine.TabIndex = 4;
            // 
            // typingTestTimer
            // 
            this.typingTestTimer.Interval = 1000;

            this.tblpMainTable.ResumeLayout(false);
            this.tblpMainTable.PerformLayout();
            this.tblpFooter.ResumeLayout(false);
            this.tblpFooter.PerformLayout();
            this.tblpButtonsRestartAndBack.ResumeLayout(false);
            this.tblpTimeElapsedSeconds.ResumeLayout(false);
            this.tblpTimeElapsedSeconds.PerformLayout();
            this.tblpWPM.ResumeLayout(false);
            this.tblpWPM.PerformLayout();
        }

        #endregion
        public System.Windows.Forms.FlowLayoutPanel flpWordsToType { get; private set; }
        public System.Windows.Forms.Button btnBack { get; private set; }
        public System.Windows.Forms.Label lblTimeLabel { get; private set; }
        public System.Windows.Forms.Label lblSeconrdsLabel { get; private set; }
        public System.Windows.Forms.TableLayoutPanel tblpMainTable { get; private set; }
        public System.Windows.Forms.TextBox tbInputLine { get; private set; }
        public System.Windows.Forms.TableLayoutPanel tblpFooter { get; private set; }
        public System.Windows.Forms.TextBox tbTimeElapsed { get; private set; }
        public System.Windows.Forms.TableLayoutPanel tblpTimeElapsedSeconds { get; private set; }
        public System.Windows.Forms.TableLayoutPanel tblpWPM { get; private set; }
        public System.Windows.Forms.Label lblWPMText { get; private set; }
        public System.Windows.Forms.Label lblWPMValue { get; private set; }
        public System.Windows.Forms.Timer typingTestTimer { get; private set; }
        public System.Windows.Forms.TableLayoutPanel tblpButtonsRestartAndBack { get; private set; }
        public System.Windows.Forms.Button btnRestart { get; private set; }
    }
}

