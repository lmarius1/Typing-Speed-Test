﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypingSpeedTest
{
    public class Test
    {
        public string TestName { get; set; }
        public string[] TestBody { get; set; }  //Each line of an array represents a paragraph

        public Test(string testName, string[] testBody)
        {
            TestName = testName;
            TestBody = testBody;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Test))
                return false;

            //Test names must be unique because they are saved in one location as TestName.txt. Hence they can be compared
            //Test body is also compared because if body of the text was changed, then two Tests are not equal
            Test other = (Test)obj;
            return StringComparer.Ordinal.Equals(TestName, other.TestName) &&
                StringComparer.Ordinal.Equals(string.Join("", TestBody), string.Join("", other.TestBody));
        }

        public override int GetHashCode()
        {
            //Classes are equal based on the TestName and Body, so generating a hash code based on that.
            return TestName.GetHashCode() ^ (string.Join("", TestBody)).GetHashCode();
        }
    }
}
