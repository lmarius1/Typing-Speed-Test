﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TypingSpeedTest
{
    public class TypingTest : TypingTestTableLayoutPanelDesigner
    {
        private class Node
        {
            public RichTextBox Rtb { get; set; }
            public Node Next { get; set; }
            public Node Prev { get; set; }

            public Node(RichTextBox rtb, Node prev, Node next)
            {
                Rtb = rtb;
                Next = next;
                Prev = prev;
            }
        }

        private Node head;
        private Node current;

        public delegate void BackButtonClicked();
        private BackButtonClicked backButtonClicked;

        private int currentIndex = 0;   //Index of the current character of the word
        private int nrOfIncorrectCharacters = 0;
        private int nrOfCorrectCharacters = 0;

        //"WPM" is just the corrected CPM divided by 5. That's a de facto international standard.
        //https://typing-speed-test.aoeu.eu/
        private const int charactersPerMinToWordsPerMinDivisor = 5;
        
        private readonly Font fontWithoutUnderline = new Font("Consolas", 14);
        private readonly Font fontWithUnderLine = new Font("Consolas", 14, FontStyle.Underline);

        private int secondsToTest;  //Duration of the test in seconds
        private int secondsElapsed;

        private Test CurrentTest;   //The last test assigned in StartTest method.

        private Stopwatch sw;

        // Make the RichTextBox fit its contents.
        private void rchContents_ContentsResized(object sender, ContentsResizedEventArgs e)
        {
            RichTextBox rch = sender as RichTextBox;
            
            using (Graphics g = flpWordsToType.CreateGraphics())
            {
                SizeF sizes = g.MeasureString(rch.Text, rch.Font);

                rch.ClientSize = new Size(
                    (int)Math.Round(sizes.Width),
                    e.NewRectangle.Height);
            }
        }

        public TypingTest(BackButtonClicked backButtonClicked)
        {
            this.backButtonClicked = backButtonClicked;

            btnBack.Click += new System.EventHandler(btnBack_Click);
            btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            tbInputLine.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInputLine_KeyPress);
            typingTestTimer.Tick += new System.EventHandler(this.typingTestTimer_Tick);
            
            tblpMainTable.BackColor = Color.AliceBlue;
        }

        public void StartTest(Test test, int secondsToTest)
        {
            this.secondsToTest = secondsToTest;

            if (CurrentTest != null && CurrentTest.Equals(test))
            {
                RestartTest();
                return;
            }

            CurrentTest = test;
            flpWordsToType.Controls.Clear();    //clear all words from flowpanel
            head = null;
            current = null;
            ResetMainParametersExceptFlowPanel();   //reset all parameters so that textbox is empty, wpm is 0 etc.

            string[] paragraphs = test.TestBody;
            foreach (string str in paragraphs)
            {
                string[] wordsWithoutSpaces = Regex.Split(str, "\\s");

                foreach (string word in wordsWithoutSpaces)
                {
                    if (string.IsNullOrWhiteSpace(word))
                    {
                        //If there are at least two subsequent spaces in text then there will be empty rows.
                        //In that case, those empty rows are just ignored here.

                        continue;
                    }

                    var richTextBoxWord = new RichTextBox();

                    richTextBoxWord.Text = word;
                    richTextBoxWord.Font = fontWithoutUnderline;

                    richTextBoxWord.BorderStyle = BorderStyle.None;
                    richTextBoxWord.ShortcutsEnabled = false;   //disable cut,copy and paste features
                    
                    richTextBoxWord.ReadOnly = true;
                    richTextBoxWord.BackColor = Color.AliceBlue;

                    richTextBoxWord.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(
                        this.rchContents_ContentsResized);

                    richTextBoxWord.Margin = new Padding(0);
                    flpWordsToType.Controls.Add(richTextBoxWord);

                    if (head == null)
                    {
                        head = new Node(richTextBoxWord, null, null);
                        current = head;
                    }
                    else
                    {
                        current.Next = new Node(richTextBoxWord, current, null);
                        current = current.Next;
                    }
                }
                if (current != null)
                    flpWordsToType.SetFlowBreak(current.Rtb, true);
            }

            if (head == null)   //Test with empty body was given... Just disable input line.
            {
                tbInputLine.Enabled = false;
                return;
            }
            
            current = head;
            current.Rtb.Font = fontWithUnderLine;
            tbInputLine.Select();
        }

        //Makes sure that if scrollbar in panel exists, it still focuses on a word to be typed.
        private void refreshSelection(RichTextBox rtb)
        {
            rtb.Select();

            tbInputLine.Select();
            tbInputLine.SelectionStart = tbInputLine.Text.Length;
            tbInputLine.SelectionLength = 0;
        }

        private void updateWPM()
        {
            //Overall correct characters divided by divisor (which defines characters per word) and result is words per timeElapsed.
            double wordsPerTimeElapsed = (double)nrOfCorrectCharacters / charactersPerMinToWordsPerMinDivisor;

            long elapsedMilliSeconds = sw.ElapsedMilliseconds;

            //To get words per minute, 60 (as seconds) are divided by timeElapsed and then multiplied by wordsPerTimeElapsed.
            double timeRelative = (double)60 / ((double)elapsedMilliSeconds/1000);

            int wpm = (int)(wordsPerTimeElapsed * timeRelative);

            //Console.WriteLine(nrOfCorrectCharacters + " " + wordsPerTimeElapsed + " " + timeRelative + " " + wpm +
            //    " " + nrOfIncorrectCharacters);

            lblWPMValue.Text = wpm.ToString();
        }

        private void tbInputLine_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 8 || (e.KeyChar < 31 && e.KeyChar > 8) || e.KeyChar == 127)
            {
                //If KeyChar is control character then do nothing.
                return;
            }

            if (!typingTestTimer.Enabled)   //If time is not started yet - start it.
            {
                typingTestTimer.Start();
                sw = Stopwatch.StartNew();
                tbTimeElapsed.Text = secondsToTest.ToString();
            }

            int bckSpace = 8;   //backspace value from ascii table
            if (e.KeyChar == bckSpace)
            {
                if (currentIndex == 0)  //Index is focused on the first character of the word
                {
                    Console.WriteLine("True");
                    if (current.Prev != null)   //Check if previous word exists
                    {
                        current.Rtb.Font = fontWithoutUnderline;
                        current = current.Prev;

                        currentIndex = current.Rtb.Text.Length;

                        refreshSelection(current.Rtb);

                        //All the characters after any incorrect are also incorrect. 
                        //So if >0, then the character removed with backspace is certainly incorrect.
                        if (nrOfIncorrectCharacters > 0)
                        {
                            nrOfIncorrectCharacters--;
                        }
                        else
                        {
                            nrOfCorrectCharacters--;    //The empty character removed was actually "Green"
                        }
                    }
                    else
                    {
                        nrOfIncorrectCharacters = 0;   //If no characters are selected then there are no incorrect characters.
                        nrOfCorrectCharacters = 0;
                    }
                }
                else
                {
                    if (current.Rtb.Text.Length < currentIndex)
                    {
                        //Will be true when if instead of space multiple other characters were entered (when space was expected).

                        currentIndex--;
                    }
                    else
                    {
                        current.Rtb.Select(currentIndex - 1, 1);
                        current.Rtb.SelectionColor = Color.Black;
                        currentIndex--;
                    }

                    if (nrOfIncorrectCharacters > 0)
                        nrOfIncorrectCharacters--;
                    else
                        nrOfCorrectCharacters--;
                }
            }
            else
            {
                if (current.Rtb.Text.Length < currentIndex) //Too many characters entered...
                {
                    currentIndex++;
                    nrOfIncorrectCharacters++;
                }
                else if (current.Rtb.Text.Length == currentIndex)
                {
                    int space = 32;    //from ascii table
                    if (e.KeyChar == space)
                    {
                        if (current.Next != null)
                        {
                            current = current.Next;
                            current.Rtb.Font = fontWithUnderLine;
                            currentIndex = 0;

                            refreshSelection(current.Rtb);
                            tbInputLine.Text = string.Empty;
                            e.Handled = true;   //Do not write space into an empty textbox

                            if (nrOfIncorrectCharacters != 0)
                            {
                                //Allow to focus on the next word but also mark that characters are incorrect (including current space)

                                nrOfIncorrectCharacters++;
                            }
                            else
                            {
                                nrOfCorrectCharacters++;    //space should also be included into WPM.
                            }
                        }
                        else
                        {
                            //Test should be completed by the time the last character was correctly entered and NOT here.
                            //Here just update the currentIndex so that if any additional characters are inputted 
                            //into textbox, then they should be cleared with backspace first...
                            currentIndex++;
                        }
                    }
                    else
                    {
                        currentIndex++;
                        nrOfIncorrectCharacters++;
                    }
                }
                else
                {
                    char c = current.Rtb.Text[currentIndex];

                    current.Rtb.Select(currentIndex, 1);

                    if (e.KeyChar == c)
                    {
                        if (nrOfIncorrectCharacters > 0)
                        {
                            //all characters should be correct, so if any character before was incorrect - all the
                            //other characters on the right will also be incorrect until all characters are corrected.
                            nrOfIncorrectCharacters++;
                            current.Rtb.SelectionColor = Color.Red;
                        }
                        else
                        {
                            current.Rtb.SelectionColor = Color.Green;

                            nrOfCorrectCharacters++;

                            //If the checked character was the last char of the word, then if next word
                            //does not exist - handle the end of the text.
                            if (current.Rtb.Text.Length == currentIndex + 1 && current.Next == null)
                            {
                                typingTestTimer.Stop();
                                sw.Stop();
                                updateWPM();
                                tbInputLine.Enabled = false;
                                current.Rtb.Select(); //focus on this place instead of buttons...
                            }
                        }
                    }
                    else
                    {
                        current.Rtb.SelectionColor = Color.Red;
                        nrOfIncorrectCharacters++;
                    }

                    currentIndex++;
                    refreshSelection(current.Rtb);  //set focus on the current word.
                }
            }
        }

        private void typingTestTimer_Tick(object sender, EventArgs e)
        {
            secondsElapsed++;
            int secondsLeft = secondsToTest - secondsElapsed;
            tbTimeElapsed.Text = secondsLeft.ToString();
            updateWPM();

            if (secondsLeft == 0)
            {
                typingTestTimer.Stop();
                sw.Stop();
                tbInputLine.Enabled = false;
                current.Rtb.Select(); //focus on this place instead of buttons...
            }
        }

        private void ResetMainParametersExceptFlowPanel()
        {
            tbInputLine.Text = "";
            tbInputLine.Enabled = true;
            tbInputLine.Select();
            tbTimeElapsed.Text = "";
            lblWPMValue.Text = "0";

            currentIndex = 0;
            nrOfIncorrectCharacters = 0;
            nrOfCorrectCharacters = 0;

            secondsElapsed = 0;
        }

        //Reset all parameters of the test and allow to try the same test again.
        private void RestartTest()
        {
            if (head != null)
            {
                head.Rtb.Select(0, head.Rtb.Text.Length);
                head.Rtb.SelectionColor = Color.Black;
                head.Rtb.Font = fontWithUnderLine;

                if (head.Next != null)
                {
                    current = head.Next;
                    while (current != null)
                    {
                        current.Rtb.Font = fontWithoutUnderline;
                        current.Rtb.SelectionColor = Color.Black;

                        current = current.Next;
                    }
                }

                current = head;

                ResetMainParametersExceptFlowPanel();

                refreshSelection(head.Rtb);
            }
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            typingTestTimer.Stop();

            if (sw != null && sw.IsRunning)
                sw.Stop();

            RestartTest();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            //Stop timers first and only then go back
            typingTestTimer.Stop();

            if (sw != null && sw.IsRunning)
                sw.Stop();

            backButtonClicked();    //Delegate to go back.
        }
    }
}
