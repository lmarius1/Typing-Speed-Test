﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace TypingSpeedTest
{
    public partial class TypingSpeedTestMain : Form
    {
        private TypingTest typingTest;
        private string path;
        private Tests tests;
        private List<string> testNames;

        public TypingSpeedTestMain()
        {
            InitializeComponent();
            SetUpTypingTestDirectory();

            tests = new Tests(path);
            SetAvailableTypingTestsToComboBox();

            SetTestLengthOptions();

            typingTest = new TypingTest(BackButtonClickedInTypingTest);
            this.Controls.Add(typingTest.tblpMainTable);
            this.Controls[typingTest.tblpMainTable.Name].Hide();
        }

        private void SetTestLengthOptions()
        {
            List<int> length = new List<int>()
            {
                5, 10, 30, 60, 120, 180, 300, 360
            };
            cbTestLength.DataSource = length;
            cbTestLength.SelectedIndex = 3;
        }

        private void SetAvailableTypingTestsToComboBox()
        {
            testNames = tests.GetAllTestNames();
            cbSelectedTest.DataSource = testNames;
            if (testNames.Count > 0)
                cbSelectedTest.SelectedIndex = 0;
        }

        private  void SetUpTypingTestDirectory()
        {
            //Get directory from where the program is executed and append "TypingTests" directory name
            path = Path.GetFullPath(Path.Combine(
                System.Reflection.Assembly.GetExecutingAssembly().Location, "..\\", "TypingTests"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            this.Text = "Typing Test - Test";

            Test t = GetSelectedTest();
            if (t == null)
                return;

            try
            {
                this.Controls[typingTest.tblpMainTable.Name].Show();    //show UI of typingTest
                this.Controls[typingTest.tblpMainTable.Name].BringToFront();

                int selectedSeconds = Convert.ToInt32(cbTestLength.SelectedValue.ToString());
                typingTest.StartTest(t, selectedSeconds);
            } catch (Exception ex)
            {
                MessageBox.Show(this, "A problem with running a typing test:\n" + ex.ToString());
            }
        }

        private void BackButtonClickedInTypingTest()
        {
            //When back button is clicked, hide that UI and come back to start screen.
            this.Text = "Typing Test - Start";

            this.Controls[typingTest.tblpMainTable.Name].Hide();
        }

        private void btnCreateNewTest_Click(object sender, EventArgs e)
        {
            formCreateNewOrEditTest cnt = new formCreateNewOrEditTest(
                path, tests.TestWithThisTestNameDoesNotExist, HandleNewTestAdded);
            cnt.StartPosition = FormStartPosition.CenterParent;
            cnt.ShowDialog(this);
        }

        private void HandleNewTestAdded(Test t)
        {
            testNames.Add(t.TestName);
            cbSelectedTest.DataSource = null;
            cbSelectedTest.DataSource = testNames;
            tests.AddTest(t);
        }

        private void DeleteSelectedObjectFromCombobox()
        {
            testNames.RemoveAt(testNames.IndexOf(cbSelectedTest.SelectedValue.ToString()));
            cbSelectedTest.DataSource = null;
            cbSelectedTest.DataSource = testNames;
        }

        private void btnEditSelectedTest_Click(object sender, EventArgs e)
        {
            Test t = GetSelectedTest();
            if (t == null)
                return;

            formCreateNewOrEditTest cnt = new formCreateNewOrEditTest(
                path, tests.TestWithThisTestNameDoesNotExist, HandleTestUpdated, t);
            cnt.StartPosition = FormStartPosition.CenterParent;
            cnt.ShowDialog(this);
        }

        private void HandleTestUpdated(Test t)
        {
            tests.UpdateTest(t);
        }

        //If test was found - returns test, otherwise returns null
        private Test GetSelectedTest()
        {
            if (cbSelectedTest.SelectedValue == null)
            {
                MessageBox.Show(this, "No test selected.");
                return null;
            }

            string selectedTestName = cbSelectedTest.SelectedValue.ToString();

            Test t;
            try
            {
                t = tests.GetTestByName(selectedTestName);
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(this, "A problem with finding test file\nError: " + ex.ToString());
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "A problem with reading test:\n" + ex.ToString());
                return null;
            }
            return t;
        }

        private void btnDeleteSelectedTest_Click(object sender, EventArgs e)
        {
            Test t = GetSelectedTest();
            if (t == null)
                return;

            DialogResult confirmResult = MessageBox.Show(this, "Are you sure you want to delete this test?",
                                     "Confirm Delete!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                string file = Path.Combine(path, t.TestName + ".txt");
                if (File.Exists(file))
                {
                    try
                    {
                        FileSystem.DeleteFile(
                              file,
                              UIOption.OnlyErrorDialogs,
                              RecycleOption.SendToRecycleBin,
                              UICancelOption.ThrowException);

                        tests.RemoveTest(t);

                        DeleteSelectedObjectFromCombobox();

                        MessageBox.Show(this, "A test '" + t.TestName + "' was successfully deleted.");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "An error occured while trying to delete a test.\nError: " +
                            ex.ToString());
                    }
                }
                else
                {
                    MessageBox.Show(this, "Cannot delete a test because the test file was not found.\n" +
                        "File with full path it is expected to be at: " + file);
                }
            }
        }
    }
}
