﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypingSpeedTest
{
    class Tests
    {
        private Dictionary<string, Test> TestsInFolder;
        private DirectoryInfo di;

        public Tests(string pathToTypingTestsFolder)
        {
            TestsInFolder = new Dictionary<string, Test>();

            di = new DirectoryInfo(pathToTypingTestsFolder);
            ScanAllTxtFilesInDirectory();
        }

        public List<string> GetAllTestNames()
        {
            return TestsInFolder.Keys.ToList();
        }

        //Should be called only if it certainly does not already exist...
        public void AddTest(Test t)
        {
            TestsInFolder.Add(t.TestName.ToLower(), t);
        }

        public void UpdateTest(Test t)
        {
            TestsInFolder[t.TestName.ToLower()] = t;
        }

        public void RemoveTest(Test t)
        {
            TestsInFolder.Remove(t.TestName);
        }

        private void ScanAllTxtFilesInDirectory()
        {
            FileInfo[] files = di.GetFiles("*.txt");    //read all txt files from the directory
            foreach (FileInfo file in files)
            {
                //Add file name to dictionary only (Test as a value will be added only when the specific test is selected)
                if (!TestsInFolder.ContainsKey((Path.GetFileNameWithoutExtension(file.Name)).ToLower()))
                {
                    TestsInFolder.Add((Path.GetFileNameWithoutExtension(file.Name)).ToLower(), null);
                }
            }
        }

        public bool TestWithThisTestNameDoesNotExist(string testName)
        {
            return !TestsInFolder.ContainsKey(testName.ToLower());
        }

        private Test GetTestByNameOrReturnNULL(string testName)
        {
            string testNameToLower = testName.ToLower();
            if (TestsInFolder.ContainsKey(testNameToLower))
            {
                if (TestsInFolder[testNameToLower] == null)
                {
                    //File was not opened yet - read the text

                    string[] body = File.ReadAllLines(Path.Combine(di.FullName, testNameToLower + ".txt"));
                    Test t = new Test(testNameToLower, body);
                    TestsInFolder[testNameToLower] = t;
                }
                return TestsInFolder[testNameToLower];
            } else
            {
                return null;
            }
        }

        public Test GetTestByName(string testName)
        {
            string testNameToLower = testName.ToLower();
            Test t = GetTestByNameOrReturnNULL(testNameToLower);
            if (t != null)
            {
                return t;
            } else
            {
                ScanAllTxtFilesInDirectory();   //check again - maybe file was added during runtime manually (i.e. not from the program)
                Test t2 = GetTestByNameOrReturnNULL(testNameToLower);
                if (t2 != null)
                    return t2;
                else
                    throw new FileNotFoundException("Test file not found.\nFile name with location: " + 
                        Path.Combine(di.FullName, testNameToLower + ".txt"));
            }
        }
    }
}
