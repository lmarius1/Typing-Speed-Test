﻿namespace TypingSpeedTest
{
    partial class TypingSpeedTestMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartTest = new System.Windows.Forms.Button();
            this.cbSelectedTest = new System.Windows.Forms.ComboBox();
            this.btnCreateNewTest = new System.Windows.Forms.Button();
            this.btnEditSelectedTest = new System.Windows.Forms.Button();
            this.lblSelectTestLabel = new System.Windows.Forms.Label();
            this.cbTestLength = new System.Windows.Forms.ComboBox();
            this.lblTestLengthLabel = new System.Windows.Forms.Label();
            this.tblpStartScreen = new System.Windows.Forms.TableLayoutPanel();
            this.tblpButtonsForTest = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteSelectedTest = new System.Windows.Forms.Button();
            this.tblpStartScreen.SuspendLayout();
            this.tblpButtonsForTest.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartTest
            // 
            this.btnStartTest.Location = new System.Drawing.Point(3, 3);
            this.btnStartTest.MinimumSize = new System.Drawing.Size(103, 27);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(103, 27);
            this.btnStartTest.TabIndex = 0;
            this.btnStartTest.Text = "Take a typing test";
            this.btnStartTest.UseVisualStyleBackColor = true;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // cbSelectedTest
            // 
            this.cbSelectedTest.BackColor = System.Drawing.SystemColors.Window;
            this.cbSelectedTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSelectedTest.FormattingEnabled = true;
            this.cbSelectedTest.Location = new System.Drawing.Point(76, 143);
            this.cbSelectedTest.Name = "cbSelectedTest";
            this.cbSelectedTest.Size = new System.Drawing.Size(121, 21);
            this.cbSelectedTest.TabIndex = 1;
            // 
            // btnCreateNewTest
            // 
            this.btnCreateNewTest.Location = new System.Drawing.Point(3, 35);
            this.btnCreateNewTest.MinimumSize = new System.Drawing.Size(103, 27);
            this.btnCreateNewTest.Name = "btnCreateNewTest";
            this.btnCreateNewTest.Size = new System.Drawing.Size(103, 27);
            this.btnCreateNewTest.TabIndex = 2;
            this.btnCreateNewTest.Text = "Create New";
            this.btnCreateNewTest.UseVisualStyleBackColor = true;
            this.btnCreateNewTest.Click += new System.EventHandler(this.btnCreateNewTest_Click);
            // 
            // btnEditSelectedTest
            // 
            this.btnEditSelectedTest.Location = new System.Drawing.Point(3, 67);
            this.btnEditSelectedTest.MinimumSize = new System.Drawing.Size(103, 27);
            this.btnEditSelectedTest.Name = "btnEditSelectedTest";
            this.btnEditSelectedTest.Size = new System.Drawing.Size(103, 27);
            this.btnEditSelectedTest.TabIndex = 3;
            this.btnEditSelectedTest.Text = "Edit";
            this.btnEditSelectedTest.UseVisualStyleBackColor = true;
            this.btnEditSelectedTest.Click += new System.EventHandler(this.btnEditSelectedTest_Click);
            // 
            // lblSelectTestLabel
            // 
            this.lblSelectTestLabel.AutoSize = true;
            this.lblSelectTestLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblSelectTestLabel.Location = new System.Drawing.Point(76, 127);
            this.lblSelectTestLabel.Name = "lblSelectTestLabel";
            this.lblSelectTestLabel.Size = new System.Drawing.Size(125, 13);
            this.lblSelectTestLabel.TabIndex = 4;
            this.lblSelectTestLabel.Text = "Please select a test";
            this.lblSelectTestLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // cbTestLength
            // 
            this.cbTestLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTestLength.FormattingEnabled = true;
            this.cbTestLength.Location = new System.Drawing.Point(207, 143);
            this.cbTestLength.Name = "cbTestLength";
            this.cbTestLength.Size = new System.Drawing.Size(121, 21);
            this.cbTestLength.TabIndex = 5;
            // 
            // lblTestLengthLabel
            // 
            this.lblTestLengthLabel.AutoSize = true;
            this.lblTestLengthLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTestLengthLabel.Location = new System.Drawing.Point(207, 127);
            this.lblTestLengthLabel.Name = "lblTestLengthLabel";
            this.lblTestLengthLabel.Size = new System.Drawing.Size(125, 13);
            this.lblTestLengthLabel.TabIndex = 6;
            this.lblTestLengthLabel.Text = "Test length in seconds";
            this.lblTestLengthLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tblpStartScreen
            // 
            this.tblpStartScreen.ColumnCount = 5;
            this.tblpStartScreen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpStartScreen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tblpStartScreen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tblpStartScreen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tblpStartScreen.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpStartScreen.Controls.Add(this.tblpButtonsForTest, 3, 2);
            this.tblpStartScreen.Controls.Add(this.lblSelectTestLabel, 1, 1);
            this.tblpStartScreen.Controls.Add(this.cbTestLength, 2, 2);
            this.tblpStartScreen.Controls.Add(this.lblTestLengthLabel, 2, 1);
            this.tblpStartScreen.Controls.Add(this.cbSelectedTest, 1, 2);
            this.tblpStartScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblpStartScreen.Location = new System.Drawing.Point(0, 0);
            this.tblpStartScreen.Name = "tblpStartScreen";
            this.tblpStartScreen.RowCount = 4;
            this.tblpStartScreen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpStartScreen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tblpStartScreen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tblpStartScreen.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblpStartScreen.Size = new System.Drawing.Size(540, 398);
            this.tblpStartScreen.TabIndex = 7;
            // 
            // tblpButtonsForTest
            // 
            this.tblpButtonsForTest.ColumnCount = 1;
            this.tblpButtonsForTest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblpButtonsForTest.Controls.Add(this.btnStartTest, 0, 0);
            this.tblpButtonsForTest.Controls.Add(this.btnCreateNewTest, 0, 1);
            this.tblpButtonsForTest.Controls.Add(this.btnEditSelectedTest, 0, 2);
            this.tblpButtonsForTest.Controls.Add(this.btnDeleteSelectedTest, 0, 3);
            this.tblpButtonsForTest.Location = new System.Drawing.Point(338, 140);
            this.tblpButtonsForTest.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tblpButtonsForTest.Name = "tblpButtonsForTest";
            this.tblpButtonsForTest.RowCount = 4;
            this.tblpButtonsForTest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblpButtonsForTest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblpButtonsForTest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblpButtonsForTest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblpButtonsForTest.Size = new System.Drawing.Size(125, 129);
            this.tblpButtonsForTest.TabIndex = 8;
            // 
            // btnDeleteSelectedTest
            // 
            this.btnDeleteSelectedTest.Location = new System.Drawing.Point(3, 99);
            this.btnDeleteSelectedTest.MinimumSize = new System.Drawing.Size(103, 27);
            this.btnDeleteSelectedTest.Name = "btnDeleteSelectedTest";
            this.btnDeleteSelectedTest.Size = new System.Drawing.Size(103, 27);
            this.btnDeleteSelectedTest.TabIndex = 4;
            this.btnDeleteSelectedTest.Text = "Delete";
            this.btnDeleteSelectedTest.UseVisualStyleBackColor = true;
            this.btnDeleteSelectedTest.Click += new System.EventHandler(this.btnDeleteSelectedTest_Click);
            // 
            // TypingSpeedTestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(540, 398);
            this.Controls.Add(this.tblpStartScreen);
            this.MinimumSize = new System.Drawing.Size(420, 250);
            this.Name = "TypingSpeedTestMain";
            this.Text = "Typing Test - Start";
            this.tblpStartScreen.ResumeLayout(false);
            this.tblpStartScreen.PerformLayout();
            this.tblpButtonsForTest.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.ComboBox cbSelectedTest;
        private System.Windows.Forms.Button btnCreateNewTest;
        private System.Windows.Forms.Button btnEditSelectedTest;
        private System.Windows.Forms.Label lblSelectTestLabel;
        private System.Windows.Forms.ComboBox cbTestLength;
        private System.Windows.Forms.Label lblTestLengthLabel;
        private System.Windows.Forms.TableLayoutPanel tblpStartScreen;
        private System.Windows.Forms.TableLayoutPanel tblpButtonsForTest;
        private System.Windows.Forms.Button btnDeleteSelectedTest;
    }
}

