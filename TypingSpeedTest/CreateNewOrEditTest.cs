﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TypingSpeedTest
{
    public partial class formCreateNewOrEditTest : Form
    {
        //a delegate method to check whether test with the specified test name does not exist
        private Func<string, bool> TestWithThisTestNameDoesNotExist;

        //E.g. update combobox items and add a new Test to Tests class.
        private Action<Test> TestWasSavedUpdateInformation;

        //If no test to edit was passed with the constructor, then expecting to create a new test.
        private Test TestToEdit;

        private DirectoryInfo DirectoryWhereTestsAreSaved;

        public formCreateNewOrEditTest(string pathToDirectoryWhereTestsAreSaved, 
            Func<string, bool> testWithThisTestNameDoesNotExist,
            Action<Test> testWasSavedUpdateInformation)
        {
            InitializeComponent();

            this.TestWithThisTestNameDoesNotExist = testWithThisTestNameDoesNotExist;
            TestWasSavedUpdateInformation = testWasSavedUpdateInformation;
            DirectoryWhereTestsAreSaved = new DirectoryInfo(pathToDirectoryWhereTestsAreSaved);
        }

        public formCreateNewOrEditTest(string directoryWhereTestsAreSaved,
            Func<string, bool> testWithThisTestNameDoesNotExist,
            Action<Test> testWasSavedUpdateInformation, Test testToEdit) :
                this(directoryWhereTestsAreSaved, testWithThisTestNameDoesNotExist, testWasSavedUpdateInformation)
        {
            TestToEdit = testToEdit;
            tbTestName.Text = testToEdit.TestName;
            tbTestName.Enabled = false;
            tbTestContent.Text = string.Join(Environment.NewLine, testToEdit.TestBody);
            this.Text = "Edit test";

            FocusOnTheStartOfTheText();
        }

        //Sets focus on the start of the text
        private void FocusOnTheStartOfTheText()
        {
            tbTestContent.Select();
            tbTestContent.SelectionStart = 0;
            tbTestContent.SelectionLength = 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (TestToEdit == null) 
            {
                //Check whether a TestName is valid (i.e. does not exist and also name is valid for saving)

                if (!TestWithThisTestNameDoesNotExist(tbTestName.Text))
                {
                    MessageBox.Show(this, "Test with name " + tbTestName.Text + " already exists");
                    return;
                }

                string fileName = Path.Combine(DirectoryWhereTestsAreSaved.FullName, tbTestName.Text + ".txt");
                string isValidPath = IsValidFullPath(fileName);
                if (!isValidPath.Equals(""))
                {
                    MessageBox.Show(this, "Invalid path name.\n" + isValidPath);
                    return;
                }
            }
            
            if (string.IsNullOrWhiteSpace(tbTestContent.Text))
            {
                MessageBox.Show(this, "Test content is empty. Write something and try again.");
                return;
            }

            //If file is successfully saved then inform parent form about a new (or updated) test.
            if (TryToWriteContentsToFile())
            {
                Test t = new Test(tbTestName.Text, (tbTestContent.Text).Split('\n'));
                TestWasSavedUpdateInformation(t);

                MessageBox.Show(this, "Test was successfuly saved.");
                this.Close();
            }
        }

        private bool TryToWriteContentsToFile()
        {
            try
            {
                string fileName = Path.Combine(DirectoryWhereTestsAreSaved.FullName, tbTestName.Text + ".txt");
                File.WriteAllText(fileName, tbTestContent.Text);

                return true;
            } catch (IOException ex)
            {
                MessageBox.Show(this, "IO exception occured when trying to save a test.\n" + ex.ToString());
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "An error occured when trying to save a test.\n" + ex.ToString());
                return false;
            }
        }

        private string IsValidFullPath(string fullPath)
        {
            if (string.IsNullOrWhiteSpace(fullPath))
                return "Path is null, empty or white space.";

            bool pathContainsInvalidChars = fullPath.IndexOfAny(Path.GetInvalidPathChars()) != -1;
            if (pathContainsInvalidChars)
                return "Path contains invalid characters.";

            string fileName = Path.GetFileName(fullPath);
            if (string.IsNullOrWhiteSpace(fileName))
                return "Path must contain a file name.";

            bool fileNameContainsInvalidChars = fileName.IndexOfAny(Path.GetInvalidFileNameChars()) != -1;
            if (fileNameContainsInvalidChars)
                return "File name contains invalid characters.";

            if (!Path.IsPathRooted(fullPath))
                return "The path must be absolute.";

            return "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
